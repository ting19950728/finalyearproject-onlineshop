const mongoose = require('mongoose');
const schema = mongoose.Schema;

var userModel = new schema({
 username: {type: String, required: true, index: true},
 password: {type: String, required: true},
 firstName: {type: String},
 lastName: {type: String},
 gender: {type: String},
 age: {type: Number},
 habits: [String]
});

module.exports = mongoose.model('User', userModel);