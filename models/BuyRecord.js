const mongoose = require('mongoose');
const schema = mongoose.Schema;
const userModel = require('./User');
const itemModel = require('./Item');

var buyRecordModel = new schema({
 user: {type: schema.Types.ObjectId, ref: 'User', required: true},
 item: {type: schema.Types.ObjectId, ref: 'Item', required: true},
 recordTime: {type: schema.Types.Date, required: true} 
});

module.exports = mongoose.model('BuyRecord', buyRecordModel);