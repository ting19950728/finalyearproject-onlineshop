const mongoose = require('mongoose');
const schema = mongoose.Schema;

var categoryModel = new schema({
 name: {type:String, required: true, index: true}
});

module.exports = mongoose.model('Category', categoryModel);