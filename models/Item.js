const mongoose = require('mongoose');
const schema = mongoose.Schema;
const categoriesModel = require('./Categories');

var itemModel = new schema({
 title: {type: String, required: true},
 category: {type: schema.Types.ObjectId, ref: 'Category'},
 price: {type: Number, required:true},
 description: {type: String},
 imgsrc: {type: String},
 tags: [String]
});

module.exports = mongoose.model('Item', itemModel);