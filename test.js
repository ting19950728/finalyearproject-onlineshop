var userModel = require('./models/User');
var itemModel = require('./models/Item');
var categoryModel = require('./models/Categories');
var browseModel = require('./models/BrowseRecord');
var genders = ['M','F'];
var categories = ['Toy', 'Accessory', 'Phone Case', 'Sticker', 'Cloth'];

exports.getUserRating = function(targetUser){
 var allTags = [];
 var genderDataSet = {
  'M': {
   'Category': [],
   'Price': [],
   'Tags': [],
   'Count': 0
  },
  'F': {
   'Category': [],
   'Price': [],
   'Tags': [],
   'Count': 0
  }
 };
 var habits = [];
 browseModel.find({})
 .populate({
  path: 'user'
 })
 .populate({
  path: 'item',
  populate: {
   path: 'category'
  }
 })
 .exec()
 .then((doc) => {
  doc.forEach((v) => {
   let user = v.user;
   let item = v.item;
   let category = item.category;
   let tags = item.tags;
   let gender = user.gender;
   // Gender
   genderDataSet[gender].Count++;
   if(!genderDataSet[gender].Category[category.name])
    genderDataSet[gender].Category[category.name] = 0;
   genderDataSet[gender].Category[category.name]++;
   tags.forEach((t) => {
    if(!genderDataSet[gender].Tags[t])
     genderDataSet[gender].Tags[t] = 0;
    genderDataSet[gender].Tags[t]++;
   })

   // Habits
   user.habits.forEach((v) => {
    if(!habits[v])
     habits[v] = {
      'Category': [],
      'Price': [],
      'Tags': [],
      'Count': 0
     };
    habits[v].Count++;
    // Category
    if(!habits[v].Category[category.name])
     habits[v].Category[category.name] = 0;
    habits[v].Category[category.name]++;

    // Tags
    tags.forEach((t) => {
     if(allTags.indexOf(t) == -1) allTags.push(t);
     if(!habits[v].Tags[t])
      habits[v].Tags[t] = 0;
     habits[v].Tags[t]++;
    });
   }); // End of Habits
  });
  return doc;
 })
 .then((doc) => {
  for(let gender in genderDataSet){
  let count = genderDataSet[gender].Count;
   for(let prop in genderDataSet[gender]){
    if(prop!="Count"){
     for(let k in genderDataSet[gender][prop]){
      let length = Object.keys(genderDataSet[gender][prop]).length;
      genderDataSet[gender][prop][k] /= count / length  * 5;
     };
    }
   };
  };

  for(let habit in habits){
   let count = habits[habit].Count;
    for(let prop in habits[habit]){
     if(prop!="Count"){
      for(let k in habits[habit][prop]){
       let length = Object.keys(habits[habit][prop]).length;
       habits[habit][prop][k] /= count / length  * 5;
      };
     }
    };
   };
  let rating = {
   'Category' : [],
   'Tags': []
 };
  
  categories.forEach((v, k) => {
   rating['Category'][v] = (genderDataSet[targetUser.gender]['Category'][v] ? genderDataSet[targetUser.gender]['Category'][v] : 0);
   targetUser.habits.forEach((h) => {
    rating['Category'][v] += (habits[h]['Category'][v] ? habits[h]['Category'][v] : 0);
   })
   rating['Category'][v] /= (1 + targetUser.habits.length);
  });

  allTags.forEach((v, k) => {
   rating['Habit'][v] = (genderDataSet[targetUser.gender]['Tags'][v] ? genderDataSet[targetUser.gender]['Tags'][v] : 0);
   targetUser.habits.forEach((h) => {
    rating['Habit'][v] += (allTags[h]['Tags'][v] ? allTags[h]['Tags'][v] : 0);
   })
   rating['Habit'][v] /= (1 + targetUser.habits.length);
  });

  return rating;
 })
}