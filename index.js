var express = require('express');
var session = require('express-session');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var router = express.Router();
var morgan = require('morgan');

// initialize variables
const port = 3000;
const dburl = require('./models/dbinfo').url;
const sessionKey = {
 name: 'sessionKey',
 secret: 'fyp',
 resave: false,
 saveUninitialized: false,
 cookie:{ secure: false, maxAge: 1000 * 60 * 60}
};

// middle ware 
app.use(morgan('tiny'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(session(sessionKey));
// middle ware

// set static path
app.use(express.static(__dirname + '/www'));

// db setup
mongoose.connect(dburl, {useNewUrlParser: true});
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// set routing
const userRoute = require('./routes/users');
const categoryRoute = require('./routes/categories');
const itemRoute = require('./routes/items');
const browseRoute = require('./routes/browseRecords');
const buyRoute = require('./routes/buyRecords');
app.use('/users', userRoute);
app.use('/categories', categoryRoute);
app.use('/items', itemRoute);
app.use('/browseRecords', browseRoute);
app.use('/buyRecords', buyRoute);

const daRoute = require('./routes/dataAnalysis');
app.use('/dataAnalysis', daRoute);
// const caRoute = require('./routes/buyAnalysis');
// app.use('/buyAnalysis', caRoute);
// run server

var server = app.listen(port, function(){
 console.log(`The server is started. (listening on port ${port})`);
})