// Qminer
const qm = require('qminer');

// Models
const userModel = require('../models/User');
const itemModel = require('../models/Item');
const categoryModel = require('../models/Categories');
const browseRecordModel = require('../models/BrowseRecord');
const genders = ['M','F'];
const categories = ['Toy', 'Accessory', 'Phone Case', 'Sticker', 'Cloth'];

exports.userHabits = function(callback){
 let users = [];
 userModel.find({})
 .exec()
 .then((doc) => {
  users = doc;
  let base = new qm.Base({
   mode: 'createClean',
   schema: [{
    name: 'userHabits',
    fields: [{ name: 'habit', type: 'string'}]
    //keys: [{field: "habit", type: 'text'}]
   }]
  });

  let habitStore = base.store('userHabits');
  users.forEach((usr) => {
   let str = usr.habits.join(' ');
   habitStore.push({habit: str});
  });
  
  let distribution = habitStore.allRecords.aggr({
   name: "habit", type: "keywords", field: "habit"
  });
  callback(distribution.keywords)
 })
 .catch((err) => {
  throw err;
 })
}

exports.categoryHabits = function(category, callback){
 let base = new qm.Base({
  mode: 'createClean',
  schema: [{
   name: 'categoryHabits',
   fields: [{ name: 'habit', type: 'string'}]
   //keys: [{field: "habit", type: 'text'}]
  }]
 });
 let habitStore = base.store('categoryHabits');
 categoryModel.find({
  name: category
 }).exec()
 .then((doc) => {
  let cat = doc[0];
  browseRecordModel.find({})
  .populate({
   path: 'item',
   match: {category: cat._id}
  })
  .populate('user')
  .exec()
  .then((doc) => {
   doc.forEach((row) => {
    let user = row.user;
    habitStore.push({habit: user.habits.join(' ')});
   })
   let distribution = habitStore.allRecords.aggr({
    name: "habit", type: "keywords", field: "habit"
   })
   callback(distribution.keywords)
  })
  .catch((err) => {
   throw err;
  });
 })
 .catch((err) => {
  throw err;
 })
}

exports.habitCategories = function(habit, callback){
 let base = new qm.Base({
  mode: 'createClean',
  schema: [{
   name: 'habitCategories',
   fields: [{ name: 'category', type: 'string'}]
   //keys: [{field: "habit", type: 'text'}]
  }]
 });
 let categoryStore = base.store('habitCategories');
 browseRecordModel.find({})
 .populate({
  path: 'user',
  match: {habits: habit}
 })
 .populate({
  path: 'item',
  populate:{
   path: 'category'
  }
 })
 .exec()
 .then((doc) => {
  doc.forEach((row) => {
   categoryStore.push({category: row.item.category.name})
  });
  let distribution = categoryStore.allRecords.aggr({
   name: "category", type: "keywords", field: "category"
  })
  callback(distribution.keywords);
 })
 .catch((err) => {
  throw err;
 })
}

exports.genderCategories = function(gender, callback){
 let base = new qm.Base({
  mode: 'createClean',
  schema: [{
   name: 'genderCategories',
   fields: [{ name: 'category', type: 'string'}]
   //keys: [{field: "habit", type: 'text'}]
  }]
 });
 let categoryStore = base.store('genderCategories');
 browseRecordModel.find({})
 .populate({
  path: 'user',
  match: {gender: gender}
 })
 .populate({
  path: 'item',
  populate: {
   path: 'category'
  }
 })
 .exec()
 .then((doc) => {
  doc.forEach((row) => {
   categoryStore.push({category: row.item.category.name.replace(' ','')});
  });
  let distribution = categoryStore.allRecords.aggr({
   name: "category", type: "keywords", field: "category"
  })
  callback(distribution.keywords);
 })
 .catch((err) => {
  throw err;
 });
}

exports.getUserRating = function(targetUser, callback){
 var allTags = [];
 var genderDataSet = {
  'M': {
   'Category': [],
   'Price': [],
   'Tags': [],
   'Count': 0
  },
  'F': {
   'Category': [],
   'Price': [],
   'Tags': [],
   'Count': 0
  }
 };
 var habits = [];
 browseRecordModel.find({})
 .populate({
  path: 'user'
 })
 .populate({
  path: 'item',
  populate: {
   path: 'category'
  }
 })
 .exec()
 .then((doc) => {
  doc.forEach((v) => {
   let user = v.user;
   let item = v.item;
   let category = item.category;
   let tags = item.tags;
   let gender = user.gender;
   // Gender
   genderDataSet[gender].Count++;
   if(!genderDataSet[gender].Category[category.name])
    genderDataSet[gender].Category[category.name] = 0;
   genderDataSet[gender].Category[category.name]++;
   tags.forEach((t) => {
    if(!genderDataSet[gender].Tags[t])
     genderDataSet[gender].Tags[t] = 0;
    genderDataSet[gender].Tags[t]++;
   })

   // Habits
   user.habits.forEach((v) => {
    if(!habits[v])
     habits[v] = {
      'Category': [],
      'Price': [],
      'Tags': [],
      'Count': 0
     };
    habits[v].Count++;
    // Category
    if(!habits[v].Category[category.name])
     habits[v].Category[category.name] = 0;
    habits[v].Category[category.name]++;

    // Tags
    tags.forEach((t) => {
     if(allTags.indexOf(t) == -1) allTags.push(t);
     if(!habits[v].Tags[t])
      habits[v].Tags[t] = 0;
     habits[v].Tags[t]++;
    });
   }); // End of Habits
  });
  return doc;
 })
 .then((doc) => {
  for(let gender in genderDataSet){
  let count = genderDataSet[gender].Count;
   for(let prop in genderDataSet[gender]){
    if(prop!="Count"){
     for(let k in genderDataSet[gender][prop]){
      let length = Object.keys(genderDataSet[gender][prop]).length;
      genderDataSet[gender][prop][k] /= count / length  * 5;
     };
    }
   };
  };

  for(let habit in habits){
   let count = habits[habit].Count;
    for(let prop in habits[habit]){
     if(prop!="Count"){
      for(let k in habits[habit][prop]){
       let length = Object.keys(habits[habit][prop]).length;
       habits[habit][prop][k] /= count / length  * 5;
      };
     }
    };
   };
  let rating = {
   'Category' : [],
   'Tags': []
 };
  categories.forEach((v, k) => {
   rate = (genderDataSet[targetUser.gender]['Category'][v] ? genderDataSet[targetUser.gender]['Category'][v] : 0);
   targetUser.habits.forEach((h) => {
    rate += (habits[h]['Category'][v] ? habits[h]['Category'][v] : 0);
   })
   rate /= (1 + targetUser.habits.length);
   rateObj = {"name": v, "value": rate};
   rating['Category'].push(rateObj)
  });

  allTags.forEach((v, k) => {
   let rate = (genderDataSet[targetUser.gender]['Tags'][v] ? genderDataSet[targetUser.gender]['Tags'][v] : 0);
   targetUser.habits.forEach((h) => {
    rate += (habits[h]['Tags'][v] ? habits[h]['Tags'][v] : 0);
   })
   rate /= (1 + targetUser.habits.length);
   rateObj = {"name": v, "value": rate};
   rating['Tags'].push(rateObj);
  });
  callback(rating);
 })
}