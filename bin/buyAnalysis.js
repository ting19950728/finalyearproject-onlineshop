// Qminer
const qm = require('qminer');

// Models
const userModel = require('../models/User');
const itemModel = require('../models/Item');
const categoryModel = require('../models/Categories');
const buyRecordModel = require('../models/buyRecord');

exports.userHabits = function(callback){
 let users = [];
 userModel.find({})
 .exec()
 .then((doc) => {
  users = doc;
  let base = new qm.Base({
   mode: 'createClean',
   schema: [{
    name: 'userHabits',
    fields: [{ name: 'habit', type: 'string'}]
    //keys: [{field: "habit", type: 'text'}]
   }]
  });

  let habitStore = base.store('userHabits');
  users.forEach((usr) => {
   let str = usr.habits.join(' ');
   habitStore.push({habit: str});
  });
  
  let distribution = habitStore.allRecords.aggr({
   name: "habit", type: "keywords", field: "habit"
  });
  callback(distribution.keywords)
 })
 .catch((err) => {
  throw err;
 })
}

exports.categoryHabits = function(category, callback){
 let base = new qm.Base({
  mode: 'createClean',
  schema: [{
   name: 'categoryHabits',
   fields: [{ name: 'habit', type: 'string'}]
   //keys: [{field: "habit", type: 'text'}]
  }]
 });
 let habitStore = base.store('categoryHabits');
 categoryModel.find({
  name: category
 }).exec()
 .then((doc) => {
  let cat = doc[0];
  buyRecordModel.find({})
  .populate({
   path: 'item',
   match: {category: cat._id}
  })
  .populate('user')
  .exec()
  .then((doc) => {
   doc.forEach((row) => {
    let user = row.user;
    habitStore.push({habit: user.habits.join(' ')});
   })
   let distribution = habitStore.allRecords.aggr({
    name: "habit", type: "keywords", field: "habit"
   })
   callback(distribution.keywords)
  })
  .catch((err) => {
   throw err;
  });
 })
 .catch((err) => {
  throw err;
 })
}

exports.habitCategories = function(habit, callback){
 let base = new qm.Base({
  mode: 'createClean',
  schema: [{
   name: 'habitCategories',
   fields: [{ name: 'category', type: 'string'}]
   //keys: [{field: "habit", type: 'text'}]
  }]
 });
 let categoryStore = base.store('habitCategories');
 buyRecordModel.find({})
 .populate({
  path: 'user',
  match: {habits: habit}
 })
 .populate({
  path: 'item',
  populate:{
   path: 'category'
  }
 })
 .exec()
 .then((doc) => {
  doc.forEach((row) => {
   categoryStore.push({category: row.item.category.name})
  });
  let distribution = categoryStore.allRecords.aggr({
   name: "category", type: "keywords", field: "category"
  })
  callback(distribution.keywords);
 })
 .catch((err) => {
  throw err;
 })
}

exports.genderCategories = function(gender, callback){
 let base = new qm.Base({
  mode: 'createClean',
  schema: [{
   name: 'genderCategories',
   fields: [{ name: 'category', type: 'string'}]
   //keys: [{field: "habit", type: 'text'}]
  }]
 });
 let categoryStore = base.store('genderCategories');
 buyRecordModel.find({})
 .populate({
  path: 'user',
  match: {gender: gender}
 })
 .populate({
  path: 'item',
  populate: {
   path: 'category'
  }
 })
 .exec()
 .then((doc) => {
  doc.forEach((row) => {
   categoryStore.push({category: row.item.category.name.replace(' ','')});
  });
  let distribution = categoryStore.allRecords.aggr({
   name: "category", type: "keywords", field: "category"
  })
  callback(distribution.keywords);
 })
 .catch((err) => {
  throw err;
 });
}