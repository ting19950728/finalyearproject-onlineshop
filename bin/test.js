var mongoose = require('mongoose');
const dburl = require('../models/dbinfo').url;
// db setup
mongoose.connect(dburl, {useNewUrlParser: true});
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

let dataAnalysis = require('./buyAnalysis');
let toyResult = dataAnalysis.userHabits(function(keywords){
 keywords.forEach((obj) => {
  console.log(obj.keyword + " " + obj.weight);
 })
});