Vue.component('header-base', {
 template: '\
 <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">\
    <div class="container">\
      <a class="navbar-brand" href="#">Demo Online Shop</a>\
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">\
        <span class="navbar-toggler-icon"></span>\
      </button>\
      <div :is="navbarComponent"></div>\
    </div>\
  </nav>\
 ',
 props: [],
 data: function(){return{
  navbarComponent: "header-guest",
  user: eventHub.user
 }},
 methods: {},
 computed: {},
 created: function(){},
 mounted: function(){
   let here = this;
   eventHub.$on('isLogin', function(){
    $.ajax({
      url: '/users/isLogin',
      method: 'GET',
      success: function(data){
        if(data.result == "ok"){
          eventHub.user = data.user;
          here.navbarComponent = "header-customer";
          eventHub.$emit('refreshRecommendItemContainer');
        } else {
          here.user = undefined;
          here.navbarComponent = "header-guest";
        }
      }
    })
   });
   eventHub.$emit('isLogin');
 }
});

Vue.component('header-guest', {
 template: '\
 <div class="collapse navbar-collapse" id="navbarResponsive">\
 <ul class="navbar-nav ml-auto">\
   <li class="nav-item">\
     <a class="nav-link" href="#" data-toggle="modal" data-target="#headerModal" @click="login">Login</a>\
   </li>\
   <li class="nav-item">\
     <a class="nav-link" href="#" data-toggle="modal" data-target="#headerModal" @click="register">Register</a>\
   </li>\
 </ul>\
</div>\
',
props: [],
data: function(){return{
}},
methods: {
  login: function(){
    eventHub.$emit('loginModal');
  },
  register: function(){
    eventHub.$emit('registerModal');
  },
  dataAnalysis: function(){
    eventHub.$emit('dataAnalysisModal');
  }
},
computed: {},
created: function(){},
mounted: function(){}
});

Vue.component('header-login', {
 template: '\
 <div class="modal-content">\
   <div class="modal-header text-center bg-primary">\
    <h6 class="modal-title w-100 font-weight-bold">Sign in</h6>\
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">\
     <span aria-hidden="true">&times;</span>\
    </button>\
   </div>\
   <div class="modal-body mx-3">\
    <div class="row">\
      <div class="col-md-6">\
        <div class="form-group">\
          <input type="text" class="form-control" placeholder="Your User Name *" required v-model:"user.username" />\
        </div>\
      </div>\
      <div class="col-md-6">\
        <div class="form-group">\
          <input type="password" class="form-control" placeholder="Your Password *" required v-model="user.pwd" />\
        </div>\
      </div>\
    </div>\
    <div class="text-center">\
     <button type="button" class="btnSubmit" @click="loginSubmit" >Login</button>\
    </div>\
   </div>\
   <div class="modal-footer d-flex justify-content-center bg-secondary">\
   </div>\
  </div>\
 ',
 props: [],
 data: function(){return{
  user: {
   username: "",
   pwd: ""
  }
 }},
 methods: {
  loginSubmit: function(){
   // submit function
   let here = this;
   $.ajax({
    url: '',
    method: 'GET',
    data: here.user,
    success: function(data){
     // get response
    }
   });
  }
 },
 computed: {},
 created: function(){},
 mounted: function(){}

})

Vue.component('header-customer', {
 template: '\
 <div class="collapse navbar-collapse" id="navbarResponsive">\
 <ul class="navbar-nav ml-auto">\
  <li class="nav-item">\
     <span class="nav-link">Welcome Back, {{this.user.firstName}} {{this.user.lastName}}</span>\
   </li>\
   <li class="nav-item">\
     <a class="nav-link" href="#">Home</a>\
   </li>\
   <li class="nav-item">\
     <a class="nav-link" href="#" data-toggle="modal" data-target="#headerModal" @click="dataAnalysis">Data Analysis</a>\
   </li>\
   <li class="nav-item">\
     <a class="nav-link" href="#" @click="logout">Logout</a>\
   </li>\
 </ul>\
</div>\
',
props: [],
data: function(){return{
  user: eventHub.user
}},
methods: {
  logout: function(){
    let here = this;
    $.ajax({
      url:'/users/logout',
      method: 'GET',
      success: function(data){
        if(data.result == 'ok'){
          eventHub.$emit('isLogin');
        }
      }
    });
  },
  dataAnalysis: function(){
    eventHub.$emit('dataAnalysisModal');
  }
},
computed: {},
created: function(){},
mounted: function(){}
});
Vue.component('header-modal', {
  template: '\
  <div :id="id" class="modal" abindex="1" role="dialog" aria-hidden="true">\
    <div class="modal-dialog" role="document">\
      <div :is="modalBodyId"></div>\
    </div>\
  </div>\
  ',
  props: [],
  data: function(){return{
    id: "headerModal",
    modalBodyId: "head-modal-login"
  }},
  methods: {},
  computed: {},
  created: function(){},
  mounted: function(){
    let here = this;
    eventHub.$on('loginModal', function(){
      here.modalBodyId = "header-modal-login";
    });
    eventHub.$on('registerModal', function(){
      here.modalBodyId = "header-modal-register";
    });
    eventHub.$on('dataAnalysisModal', function(){
      here.modalBodyId = "header-modal-dataAnalysis";
    })
  }
});
Vue.component('header-modal-login',{
  template: '\
  <div class="modal-content">\
    <div class="modal-header text-center bg-primary">\
      <h4 class="modal-title w-100 font-weight-bold">Sign in</h4>\
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">\
          <span aria-hidden="true">&times;</span>\
        </button>\
      </h4>\
    </div>\
    <div class="modal-body mx-3">\
      <div class="row">\
        <div class="col-md-6">\
          <div class="form-group">\
            <input type="text" class="form-control" placeholder="Your User Name *" v-model="user.username" required />\
          </div>\
        </div>\
        <div class="col-md-6">\
          <div class="form-group">\
            <input type="password" class="form-control" placeholder="Your Password *" v-model="user.password" required />\
          </div>\
        </div>\
        <div class="text-center col-md-12">\
          <button type="button" class="btn btnSubmit" @click="loginSubmit">Login</button>\
        </div>\
      </div>\
    </div>\
    <div class="modal-footer d-flex justify-content-center bg-secondary"></div>\
  </div>\
  ',
  props: [],
  data: function(){return{
    user: {
      username: "",
      password: ""
    }
  }},
  methods: {
    clear: function(){
      this.user = {
        username: "",
        password: ""
      }
    },
    loginSubmit: function(){
      // Send Login 
      let here = this;
      let user = {
        "username": here.user.username,
        "password": here.user.password
      }
      if(user.username != "" && user.password != ""){
        $.ajax({
          url: '/users/login',
          method: 'POST',
          data: user,
          success: function(data){
            console.log(data);
            if(data.result == "ok"){
              eventHub.$emit('isLogin');
              $('#'+here.$parent.id).modal('hide');
                here.clear();
            } else {
              alert("Login error");
            }
          }
        });
      } else {
        alert("Username & password cannot be empty");
      }
    }
  },
  computed: {},
  created: function(){},
  mounted: function(){}
});
Vue.component('header-modal-register',{
  template: '\
  <div class="modal-content">\
    <div class="modal-header text-center bg-primary">\
      <h4 class="modal-title w-100 font-weight-bold">Sign in</h4>\
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">\
          <span aria-hidden="true">&times;</span>\
        </button>\
      </h4>\
    </div>\
    <div class="modal-body mx-3">\
      <div class="row">\
        <div class="col-md-6">\
          <div class="form-group">\
            <input type="text" class="form-control" placeholder="Username *" v-model="user.username" required />\
          </div>\
        </div>\
        <div class="col-md-6">\
          <div class="form-group">\
            <input type="text" class="form-control" placeholder="First Name *" min="0" v-model="user.firstName" required />\
          </div>\
        </div>\
        <div class="col-md-6">\
          <div class="form-group">\
            <input type="text" class="form-control" placeholder="Last Name *" min="0" v-model="user.lastName" required />\
          </div>\
        </div>\
        <div class="col-md-6 text-center">\
          <div class="form-group">\
            <select class="form-control" v-model="user.gender"  class="selectpicker">\
              <option v-for="g in gender" :value="g">{{g}}</option>\
            </select>\
          </div>\
        </div>\
        <div class="col-md-6">\
          <div class="form-group">\
            <input type="number" class="form-control" placeholder="Age *" min="0" v-model="user.age" required />\
          </div>\
        </div>\
        <div class="col-md-6 text-center">\
          <div class="form-group">\
            <button class="btn" @click="addHabit">+ Habit</button>\
            <input type="text" v-for="(v, k) in user.habits" class="form-control" placeholder="Habit..." v-model="user.habits[k]"/>\
          </div>\
        </div>\
        <div class="col-md-6">\
          <div class="form-group">\
            <input type="password" class="form-control" placeholder="Password *" min="0" v-model="user.password" required />\
          </div>\
        </div>\
        <div class="col-md-6">\
          <div class="form-group">\
            <input type="password" class="form-control" placeholder="Confirm Password *" min="0" v-model="confirmPassword" required />\
          </div>\
        </div>\
        <div class="text-center col-md-12">\
          <button type="button" class="btn btnSubmit" @click="registerSubmit">Register</button>\
        </div>\
      </div>\
    </div>\
    <div class="modal-footer d-flex justify-content-center bg-secondary"></div>\
  </div>\
  ',
  props: [],
  data: function(){return{
    confirmPassword: "",
    user: {
      username: "",
      password: "",
      firstName: "",
      lastName: "",
      gender: "M",
      age: 0,
      habits: new Array()
    },
    gender: ['M', 'F']
  }},
  methods: {
    addHabit: function(){
      let here = this;
      here.user.habits.push("");
    },
    clear: function(){
      this.user = {
        username: "",
        password: "",
        firstName: "",
        lastName: "",
        gender: "M",
        age: 0,
        habits: new Array()
      }
      this.confirmPassword = "";
    },
    registerSubmit: function(){
      let here = this;
      let user = here.user;
      if(user.password == here.confirmPassword){
        for(let x in user.habits){
          if(user.habits[x]==""){
            delete user.habits[x];
          }
        }
        $.ajax({
          url: "/users/register",
          method: "POST",
          data: user,
          success: function(data){
            if(data.result == "ok"){
              eventHub.$emit('isLogin');
              $('#'+here.$parent.id).modal('hide');
              here.clear();
            }
          }
        });
      }
    }
  },
  computed: {},
  created: function(){},
  mounted: function(){}
})
Vue.component('header-modal-dataAnalysis',{
  template: '\
  <div class="modal-content">\
    <div class="modal-header text-center bg-primary">\
      <h4 class="modal-title w-100 font-weight-bold">Data Analysis</h4>\
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">\
          <span aria-hidden="true">&times;</span>\
        </button>\
      </h4>\
    </div>\
    <div class="modal-body mx-3">\
      <div class="row">\
        <div class="row col-6">\
          <ul class="list-group">\
            <li class="list-group-item mt-2">\
              <button class="btn" @click="userHabits">User Habit Distribution</button>\
            </li>\
            <li class="list-group-item mt-2">\
              <select class="form-control" v-model="selectedGender">\
                <option v-for="gender in genders" :value="gender">\
                  {{gender}}\
                </option>\
              </select>\
              <button class="btn" @click="genderCategories">Find Gender : Categories</button>\
            </li>\
            <li class="list-group-item mt-2">\
              <select class="form-control" v-model="selectedCat">\
                <option v-for="category in categories" :value="category">\
                  {{category}}\
                </option>\
              </select>\
              <button class="btn" @click="categoryHabits">Find Category : Habits</button>\
            </li>\
            <li class="list-group-item mt-2">\
              <select class="form-control" v-model="selectedHabit">\
                <option v-for="habit in habits" :value="habit">\
                  {{habit}}\
                </option>\
              </select>\
              <button class="btn" @click="habitCategories">Find Habit : Categories</button>\
            </li>\
          </ul>\
        </div>\
        <div class="row col-6">\
          <ol class="list-group ml-5">\
            <li class="list-group-item mt-1" v-for="keyword in keywords">\
              {{keyword.keyword}} : {{keyword.weight.toFixed(2) * 100}}%\
            </li>\
          </ol>\
        </div>\
      </div>\
    </div>\
    <div class="modal-footer d-flex justify-content-center bg-secondary"></div>\
  </div>\
  ',
  props: [],
  data: function(){return{
      genders: ['M', 'F'],
      categories: [],
      habits: [],
      keywords: [],
      selectedCat: "",
      selectedHabit: "",
      selectedGender: "M"
  }},
  methods: {
    userHabits: function(){
      let here = this;
      here.keywords = [];
      $.ajax({
        url: '/dataAnalysis/uh',
        method: 'GET',
        success: function(data){
          if(data.result == "ok"){
            here.keywords = data.data;
          }
        }
      })
    },
    genderCategories: function(){
      let here = this;
      here.keywords = [];
      $.ajax({
        url: '/dataAnalysis/gc/' + here.selectedGender,
        method: 'GET',
        success: function(data){
          if(data.result == "ok"){
            here.keywords = data.data;
          }
        }
      })
    },
    categoryHabits: function(){
      let here = this;
      here.keywords = [];
      $.ajax({
        url: '/dataAnalysis/ch/' + here.selectedCat,
        method: 'GET',
        success: function(data){
          if(data.result == "ok"){
            here.keywords = data.data;
          }
        }
      })
    },
    habitCategories: function(){
      let here = this;
      here.keywords = [];
      $.ajax({
        url: '/dataAnalysis/hc/' + here.selectedHabit,
        method: 'GET',
        success: function(data){
          if(data.result == "ok"){
            here.keywords = data.data;
          }
        }
      })
    }
  },
  computed: {},
  created: function(){},
  mounted: function(){
    let here = this;
    $.ajax({
      url: '/categories/getAll',
      method: 'GET',
      success: function(data){
        if(data.result == 'ok'){
          data.data.forEach((v) => {
            here.categories.push(v.name);
          });
          here.selectedCat = here.categories[0];
        }
      }
    })
    $.ajax({
      url: '/users/habits',
      method: 'GET',
      success: function(data){
        if(data.result == 'ok'){
          here.habits = data.data;
          here.selectedHabit = here.habits[0];
        }
      }
    })
  }
})