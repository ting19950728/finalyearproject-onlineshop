Vue.component('body-main', {
  template: '\
  <div class="container">\
    <div class="row">\
      <body-category-menu :title="title" :categories="categories"></body-category-menu>\
      <body-item-container></body-item-container>\
    </div>\
  </div>\
  ',
  props: [],
  data: function(){return{
    title: "Demo Online Store",
    categories: []
  }},
  methods: {},
  computed: {},
  created: function(){},
  mounted: function(){
    let here = this;
    $.ajax({
      url: '/categories/getAll',
      method: 'GET',
      success: function(data){
        let categories = data.data;
        for(let x in categories){
          here.categories.push({"name": categories[x].name});
        }
      }
    })
  }
})
Vue.component('body-category-menu', {
  template: '\
  <div class="col-lg-3">\
    <h1 class="my-4">{{title}}</h1>\
    <div class="list-group">\
      <body-category-item v-for="category in categories" :category="category"></body-category-item>\
    </div>\
  </div>\
  ',
  props: ['title', 'categories'],
  data: function(){return{
  }},
  methods: {},
  computed: {},
  created: function(){},
  mounted: function(){}
});
Vue.component('body-category-item',{
  template: '\
  <a href="#" class="list-group-item" @click="callCategory">{{category.name}}</a>\
  ',
  props: ['category'],
  data: function(){return{
  }},
  methods: {
    callCategory: function(){
      let here = this;
      // Send Request and change the display category of the page
      eventHub.items = [];
      $.ajax({
        url: '/items/find/' + here.category.name,
        method: 'GET',
        success: function(data){
          eventHub.items = data.data;
          eventHub.$emit('refreshItemContainer');
        } 
      });
    }
  },
  computed: {},
  created: function(){},
  mounted: function(){}
})
Vue.component('body-item-container', {
  template: '\
  <div class="col-lg-9" style="height: 80vh; overflow-y: scroll">\
    <h4 v-if="eventHub.user">Recommended Items</h4>\
    <div class="row" v-if="eventHub.user">\
      <body-item-display v-for="item in recItems" :item="item">\
      </body-item-display>\
    </div>\
    <h4 v-if="items instanceof Array">Items</h4>\
    <div class="row">\
      <body-item-display v-for="item in items" :item="item">\
      </body-item-display>\
    </div>\
  </div>\
  ',
  props: [],
  data: function(){return{
    items: eventHub.items,
    recItems: []
  }},
  methods: {},
  computed: {},
  created: function(){},
  mounted: function(){
    let here = this;
    eventHub.items = [];
    eventHub.$on('refreshItemContainer', function(){
      here.items = eventHub.items;
    });
    eventHub.$on('refreshRecommendItemContainer', function(){
      $.ajax({
        url: '/dataAnalysis/ur',
        method: 'GET',
        success: function(data){
          if(data.result == 'ok'){
            here.recItems = data.data;
          }
        }
      })
    })
  }
})

Vue.component('body-item-display', {
 template:'\
 <div class="col-lg-4 col-md-6 mb-4">\
 <div class="card h-100">\
   <a href="#" @click="showItem"><img class="card-img-top" :src="item.imgsrc" alt=""></a>\
   <div class="card-body">\
     <h4 class="card-title">\
       <a href="#" @click="showItem">{{item.title}}</a>\
     </h4>\
     <h5>${{item.price}}</h5>\
     <p class="card-text">{{item.description}}</p>\
   </div>\
 </div>\
</div>\
',
 props: ['item'],
 data: function(){return{
 }},
 methods: {
   showItem: function(){
      let here = this;
      $.ajax({
        url: '/items/inspect/' + here.item._id,
        method: 'GET',
        success: function(data){
          eventHub.showItem = data.data;
          eventHub.$emit('showItemModal');
        }
      })
   }
 },
 computed: {},
 created: function(){},
 mounted: function(){}
});

Vue.component('body-item-modal', {
  template: '\
  <div :id="id" class="modal" abindex="1" role="dialog" aria-hidden="true">\
    <div class="modal-dialog" role="document">\
      <div class="modal-content">\
        <div class="modal-header text-center bg-primary">\
          <h4 class="modal-title w-100 font-weight-bold">Item Window</h4>\
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">\
              <span aria-hidden="true">&times;</span>\
            </button>\
          </h4>\
        </div>\
        <div class="modal-body mx-3">\
          <div class="row">\
            <div class="card h-100">\
              <img class="card-img-top" :src="item.imgsrc" alt="">\
              <div class="card-body">\
                <h4 class="card-title">\
                  {{item.title}}\
                </h4>\
                <h5>${{item.price}}</h5>\
                <p class="card-text">{{item.description}}</p>\
              </div>\
            </div>\
            <div class="text-center" v-if="eventHub.user" >\
              <button class="btn btn-alert" @click="buyThis">Buy Now</button>\
            </div>\
          </div>\
        </div>\
        <div class="modal-footer d-flex justify-content-center bg-secondary"></div>\
      </div>\
    </div>\
  </div>\
  ',
  props: [],
  data: function(){return{
    item: {},
    id: "body-item-modal"
  }},
  methods: {
    buyThis: function(){
      // Confirm here to confirm
      let here = this;
      let confirm = window.confirm('Are you confirm to buy this item');
      if(confirm){
        $.ajax({
          url: '/buyRecords/add',
          method: 'post',
          data: here.item,
          success: function(data){
            console.log(data);
            alert('Thank you for buying');
            $('#'+here.id).modal('hide');
          }
        });
      }
    },
    addBrowseRecord: function(){
      if(eventHub.user){
        let here = this;
        $.ajax({
          url: '/browseRecords/add',
          method: 'POST',
          data: here.item,
          success: function(data){
            console.log(data);
          }
        });
      }
    }
  },
  computer: {},
  created: function(){},
  mounted: function(){
    let here = this;
    eventHub.showItem = {};
    eventHub.$on('showItemModal', function(){
      here.item = eventHub.showItem;
      $('#'+here.id).modal('show');
      here.addBrowseRecord();
    })
  }
});