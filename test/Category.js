var should = require('should');
var moment = require('moment');
var categoryModel = require('../models/Categories');

describe('Category Model', () => {
 let model = {
  name: "Animal"
 }
 let category = new categoryModel(model);
 it('Properties - Name', done => {
  category.name.should.equal(model.name);
  done();
 });
})
