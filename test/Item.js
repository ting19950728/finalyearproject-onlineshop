var should = require('should');
var moment = require('moment');
var itemModel = require('../models/Item');

const mongoose = require('mongoose');
const schema = mongoose.Schema;

describe('Category Model', () => {
 let model = {
  title: "HW",
  category: new mongoose.Types.ObjectId(),
  price: 23.99,
  description: "Hello World",
  imgsrc: "123.jpg",
  tags: []
 }
 let item = new itemModel(model);
 it('Properties - Title', done => {
  item.title.should.equal(model.title);
  done();
 });
 it('Properties - Category', done => {
  item.category.should.equal(model.category);
  done();
 });
 it('Properties - Price', done => {
  item.price.should.equal(model.price);
  done();
 });
 it('Properties - Description', done => {
  item.description.should.equal(model.description);
  done();
 });
 it('Properties - Imgsrc', done => {
  item.imgsrc.should.equal(model.imgsrc);
  done();
 });
 it('Properties - Tags', done => {
  item.tags.forEach((v, k) => {
   v.should.equal(model[k]);
  });
  done();
 });
})
