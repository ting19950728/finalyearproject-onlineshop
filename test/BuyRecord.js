var should = require('should');
var moment = require('moment');
const mongoose = require('mongoose');
const schema = mongoose.Schema;
var BuyRecordModel = require('../models/BuyRecord');

describe('Category Model', () => {
 let model = {
  user: new mongoose.Types.ObjectId(),
  item: new mongoose.Types.ObjectId()
 }
 let buyRecord = new BuyRecordModel(model);
 it('Properties - User', done => {
  buyRecord.user.should.equal(model.user);
  done();
 });
 it('Properties - Item', done => {
  buyRecord.item.should.equal(model.item);
  done();
 });
})
