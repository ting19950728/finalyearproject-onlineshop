var should = require('should');
var moment = require('moment');
var userModel = require('../models/User');

describe('User Model', () => {
 let model = {
  username: 'helloWorld',
  password: '123',
  firstName: 'Louis',
  lastName: 'Wong',
  gender: 'M',
  age: 23,
  habits: ['Writing', 'Game']
 }
 let user = new userModel(model);
 it('Properties - Username', done => {
  user.username.should.equal(model.username);
  done();
 });
 it('Properties - Password', done => {
  user.password.should.equal(model.password);
  done();
 });
 it('Properties - First Name', done => {
  user.firstName.should.equal(model.firstName);
  done();
 });
 it('Properties - Last Name', done => {
  user.lastName.should.equal(model.lastName);
  done();
 });
 it('Properties - Gender', done => {
  user.gender.should.equal(model.gender);
  done();
 })
 it('Properties - Age', done => {
  user.age.should.equal(model.age);
  done();
 })
 it('Properties - Habits', done => {
  user.habits[0].should.equal(model.habits[0]);
  user.habits[1].should.equal(model.habits[1]);
  done();
 })
})
