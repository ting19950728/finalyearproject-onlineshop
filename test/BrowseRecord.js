var should = require('should');
var moment = require('moment');
const mongoose = require('mongoose');
const schema = mongoose.Schema;
var BrowseRecordModel = require('../models/BrowseRecord');

describe('Browse Record Model', () => {
 let model = {
  user: new mongoose.Types.ObjectId(),
  item: new mongoose.Types.ObjectId()
 }
 let browseRecord = new BrowseRecordModel(model);
 it('Properties - User', done => {
  browseRecord.user.should.equal(model.user);
  done();
 });
 it('Properties - Item', done => {
  browseRecord.item.should.equal(model.item);
  done();
 });
})
