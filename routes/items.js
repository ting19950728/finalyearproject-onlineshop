/*
file name: items.js
description: define RESTful API for Item Model
*/
var express = require('express');
var router = express.Router();

var itemController = require('../controllers/itemController');
router.get('/getAll', itemController.getAllItem);
router.get('/find/:category', itemController.getItemByCategory);
router.get('/inspect/:id', itemController.getItemByObjectId);
router.post('/add', itemController.addOneItem);
router.post('/new', itemController.addManyItem);

router.delete('/clearAll', itemController.clearDataBase);

module.exports = router;