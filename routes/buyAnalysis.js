var express = require('express');
var router = express.Router();

const dataAnalysisController = require('../controllers/buyAnalysisController');

router.get('/uh', dataAnalysisController.userHabit);
router.get('/ch', dataAnalysisController.categoryHabit);
router.get('/hc', dataAnalysisController.habitCategories);
router.get('/gc', dataAnalysisController.genderCategories);

module.exports = router;