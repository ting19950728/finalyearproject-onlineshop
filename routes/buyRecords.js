/*
file name: categories.js
description: define RESTful API for Category Model
*/
var express = require('express');
var router = express.Router();

const buyRecordController = require('../controllers/buyRecordController');
router.get('/getAll', buyRecordController.getAllRecord);
router.get('/findByItem/:id', buyRecordController.getItemRecord);
router.get('/findByUser/:id', buyRecordController.getUserRecord);

router.post('/add', buyRecordController.addOneRecord);
router.delete('/clearAll', buyRecordController.clearDataBase);
module.exports = router;