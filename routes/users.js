/*
file name: users.js
description: define RESTful API for User Model
*/
var express = require('express');
var router = express.Router();

var userController = require('../controllers/userController');
router.post('/login', userController.login);
router.post('/register', userController.register);
router.get('/logout', userController.logout);
router.get('/habits', userController.getAllHabits);

router.get('/isLogin', userController.isLogin);
router.get('/findAll', userController.getAllUser);
router.get('/clearAll', userController.clearDataBase);
module.exports = router;