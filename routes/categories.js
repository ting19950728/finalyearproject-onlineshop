/*
file name: categories.js
description: define RESTful API for Category Model
*/
var express = require('express');
var router = express.Router();

const categoryController = require('../controllers/categoryController');

router.get('/getAll', categoryController.getAllCategory);

router.post('/new', categoryController.addManyCategory);
router.delete('/clearAll', categoryController.clearDataBase);
module.exports = router;