var express = require('express');
var router = express.Router();

const dataAnalysisController = require('../controllers/dataAnalysisController');

router.get('/uh', dataAnalysisController.userHabit);
router.get('/ch/:category', dataAnalysisController.categoryHabit);
router.get('/hc/:habit', dataAnalysisController.habitCategories);
router.get('/gc/:gender', dataAnalysisController.genderCategories);
router.get('/ur', dataAnalysisController.userRecommendation);
module.exports = router;