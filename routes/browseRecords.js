/*
file name: categories.js
description: define RESTful API for Category Model
*/
var express = require('express');
var router = express.Router();

const browseRecordController = require('../controllers/browseRecordController');
router.get('/getAll', browseRecordController.getAllRecord);
router.get('/findByItem/:id', browseRecordController.getItemRecord);
router.get('/findByUser/:id', browseRecordController.getUserRecord);

router.post('/add', browseRecordController.addOneRecord);
router.delete('/clearAll', browseRecordController.clearDataBase);
module.exports = router;