const dataAnalysis = require('../bin/dataAnalysis');
const itemModel = require('../models/Item');
const categoryModel = require('../models/Categories');

exports.userHabit = function(req, res){
 dataAnalysis.userHabits(function(keywords){
  res.send({"result": "ok", "data": keywords});
 });
}

exports.categoryHabit = function(req, res){
 let category = req.params.category;
 dataAnalysis.categoryHabits(category, function(keywords){
  res.send({"result": "ok", "data": keywords});
 });
}

exports.habitCategories = function(req, res){
 let habit = req.params.habit;
 dataAnalysis.habitCategories(habit, function(keywords){
  res.send({"result": "ok", "data": keywords});
 });
}

exports.genderCategories = function(req, res){
 let gender = req.params.gender;
 dataAnalysis.genderCategories(gender, function(keywords){
  res.send({"result": "ok", "data": keywords});
 });
}

exports.userRecommendation = function(req, res){
 if(req.session.account){
  let user = req.session.account;
  dataAnalysis.getUserRating(user, function(rating){
   rating['Category'] = rating['Category'].sort((a, b) => a.value > b.value ? -1 : 1);
   rating['Tags'] = rating['Tags'].sort((a, b) => a.value > b.value ? -1 : 1);
   let recommendedTags = [rating['Tags'][0].name, rating['Tags'][1].name, rating['Tags'][2].name, rating['Tags'][3].name, rating['Tags'][4].name, rating['Tags'][5].name];
   let recommendItem = [];
   let recommendedCategory = [rating['Category'][0].name, rating['Category'][1].name] ;
   itemModel.find({tags: {$in: recommendedTags}})
   .populate({
    path:'category',
    match: {name: {$in: recommendedCategory}}
   })
   .exec()
   .then((doc) => {
    let usedCategory = [];
    let result = doc.filter((v) => v.category != null);
    result.forEach((v) => {
     if(usedCategory.indexOf(v.category.name) == -1){
      usedCategory.push(v.category.name);
      recommendItem.push(v);
     }
    });
    res.send({"result": "ok", "data": recommendItem});
   });
  });
 } else {
  res.send({"result": "error"})
 }
}