/*
file name: browseRecordControllers.js
description: Handle RESTful API for BrowseRecord
*/
var browseRecordModel = require('../models/BrowseRecord');
var userModel = require('../models/User');
var itemModel = require('../models/Item');
var categoryModel = require('../models/Categories');

exports.getAllRecord = function(req, res){
 // Find All Record
 browseRecordModel.find({})
 .populate('user')
 .populate('item')
 .exec((err, result)=>{
  if(err)
   throw err;
  res.send({"result": "ok", "data": "result"});
 })
 // End of Find Record
}

exports.getUserRecord = function(req, res){
 let isSelf = req.body.isSelf;
 // Check Input data
 if((isSelf && req.session.account) || req.params.id){
  // Get session account
  let user = null;
  if(isSelf && req.session.account)
   user = req.session.account;
  else
   user = req.params.id;
  // Find User
  userModel.find({username: user.username})
  .exec((err, result) => {
   if(err)
    throw err;
   if(result.length > 0){
    let uid = result[0]._id;
    // Find All Record
    browseRecordModel.find({user: uid})
    .exec((err, result) => {
     if(err)
      throw err;
     res.send({"result": "ok", "data": result});
    });
   } else {
    res.send({"result": "error"});
   } // End of Find Record
  }); // End of Find User
 } else {
  res.send({"result": "error"});
 } // End of check data
}

exports.getItemRecord = function(req, res){
 let item = req.params.id;
 // Find Item
 itemModel.find({_id: item})
 .populate('category')
 .exec((err, result) => {
  if(err)
   throw err;
  res.send({"result": "ok", "data": result});
 });
}

exports.addOneRecord = function(req, res){
 // Check Access Right
 let user = req.session.account;
 let uid = user._id;
 let item = req.body;
 // Check redundant
 if(!req.session.lastItem || req.session.lastItem._id != item._id){
  // Find User
  itemModel.find({_id: item._id})
  .exec((err, result)=>{
   if(err)
    throw err;
   if(result.length > 0){
    let record = new browseRecordModel({
     user: uid,
     item: item._id,
     recordTime: Date.now()
    });
    record.save((err) => {
     if(err)
      throw err;
     req.session.lastItem = item;
     res.send({"reuslt": "ok"});
    });
   } else {
    res.send({"result": "error", "error": "No Item"});
   }
  }); // End of Find Item
 // End of Check Access Right
 }
}

exports.clearDataBase = function(req, res){
 // Check Access Right
 browseRecordModel.deleteMany({})
 .exec((err, result) => {
  if(err)
   throw err;
  res.send({"result": "ok"});
 })
 // End 
}