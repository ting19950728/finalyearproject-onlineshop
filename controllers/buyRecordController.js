/*
file name: buyRecordControllers.js
description: Handle RESTful API for BuyRecord
*/
var buyRecordModel = require('../models/BuyRecord');
var userModel = require('../models/User');
var itemModel = require('../models/Item');
var categoryModel = require('../models/Categories');

exports.getAllRecord = function(req, res){
 // Find All Record
 buyRecordModel.find({})
 .populate('user')
 .populate('item')
 .exec((err, result)=>{
  if(err)
   throw err;
  res.send({"result": "ok", "data": "result"});
 })
 // End of Find Record
}

exports.getUserRecord = function(req, res){
 let isSelf = req.body.isSelf;
 // Check Input data
 if((isSelf && req.session.account) || req.body.user){
  // Get session account
  let user = null;
  if(isSelf && req.session.account)
   user = req.session.account;
  else
   user = req.body.user;
  // Find User
  userModel.find({username: user.username})
  .exec((err, result) => {
   if(err)
    throw err;
   if(result.length > 0){
    let uid = result[0]._id;
    // Find All Record
    buyRecordModel.find({user: uid})
    .exec((err, result) => {
     if(err)
      throw err;
     res.send({"result": "ok", "data": result});
    });
   } else {
    res.send({"result": "error"});
   } // End of Find Record
  }); // End of Find User
 } else {
  res.send({"result": "error"});
 } // End of check data
}

exports.getItemRecord = function(req, res){
 let item = req.body.item;
 // Find Item
 itemModel.find({_id: item})
 .populate('category')
 .exec((err, result) => {
  if(err)
   throw err;
  res.send({"result": "ok", "data": result});
 });
}

exports.addOneRecord = function(req, res){
 // Check Access Right
 let user = req.session.account;
 let item = req.body;
   // Find Item
   let uid = result[0]._id;
   itemModel.find({_id: item})
   .exec((err, result)=>{
    if(err)
     throw err;
    if(result.length > 0){
     let record = new buyRecordModel({
      user: uid,
      item: item,
      recordTime: Date.now()
     });
     record.save((err) => {
      if(err)
       throw err;
      res.send({"reuslt": "ok"});
     });
    } else {
     res.send({"result": "error"});
    }
   }); // End of Find Item
 // End of Check Access Right
}

exports.clearDataBase = function(req, res){
 // Check Access Right
 buyRecordModel.deleteMany({})
 .exec((err, result) => {
  if(err)
   throw err;
  res.send({"result": "ok"});
 })
 // End 
}