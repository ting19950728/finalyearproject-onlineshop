const dataAnalysis = require('../bin/buyAnalysis');

exports.userHabit = function(req, res){
 dataAnalysis.userHabits(function(keywords){
  res.send({"result": "ok", "data": keywords});
 });
}

exports.categoryHabit = function(req, res){
 let category = req.params.category;
 dataAnalysis.categoryHabits(category, function(keywords){
  res.send({"result": "ok", "data": keywords});
 });
}

exports.habitCategories = function(req, res){
 let habit = req.params.habit;
 dataAnalysis.habitCategories(habit, function(keywords){
  res.send({"result": "ok", "data": keywords});
 });
}

exports.genderCategories = function(req, res){
 let gender = req.params.gender;
 dataAnalysis.genderCategories(gender, function(keywords){
  res.send({"result": "ok", "data": keywords});
 });
}