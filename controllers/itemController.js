/*
file name: itemControllers.js
description: Handle RESTful API for Item
*/
var itemModel = require('../models/Item');
var categoryModel = require('../models/Categories');

// return: [array]

exports.getAllItem = function(req, res){
  itemModel.find({})
  .populate('category')
  .exec((err, result) => {
    if(err)
      throw err;
    res.send({"result": "ok", "data": result});
  });
}

exports.getItemByCategory = function(req, res){
  categoryModel.find({name: req.params.category})
  .exec((err, result) => {
    if(err)
      throw err;
    if(result.length < 1){
      res.send({"result": "error", "error": "No such category"});
    } else {
      let category = new categoryModel(result[0]);
      itemModel.find({category: category._id})
      .populate('category')
      .exec((err, result) => {
        if(err)
          throw err;
        res.send({"result": "ok", "data": result});
      });
    }
  });
}

exports.getItemByObjectId = function(req, res){
  let id = req.params.id;
  itemModel.find({_id: id})
  .populate('category')
  .exec((err, result) => {
    if(err)
      throw err;
    if(result.length > 0){
      res.send({"result": "ok", "data": result[0]})
    } else {
      res.send({"result": "error"});
    }
  });
}

exports.addOneItem = function(req, res){
  //Check Access Right here

  //End of Access Right
}
exports.addManyItem = function(req, res){
  //Check Access Right here
  //Do Insert Here
  let form = req.body;
  let arr = form.items;
  // Check array
  if(arr instanceof Array){
    let catArray = [];
    for(let x in arr){
      if(catArray.indexOf(arr[x].category) == -1)
        catArray.push(arr[x].category);
    }
    categoryModel.find({name: {$in: catArray}})
    .exec((err, result) => {
      if(err)
        throw err;
      if(result.length > 0){
        let catIdArr = [];
        // name => ObjectId
        for(let x in result){
          catIdArr[result[x].name] = result[x]._id;
        }
        // bind to ObjectId
        for(let x in arr){
          arr[x].category = catIdArr[arr[x].category];
        }
        // insert Bulky
        itemModel.insertMany(arr, (err, insertResult) => {
          if(err)
            throw err;
          if(insertResult.length > 0){
            res.send({"result": "ok", "count": insertResult.length});
          } else {
            // No insert
            res.send({"result": "error"});
          }
        })
      } else {
        // Category not found
        res.send({"result": "error"});
      }
    });
  } else {
    // Input data Error
    res.send({"result": "error"});
  }
  //End of Insert
  //End of Access Right
}
exports.clearDataBase = function(req, res){
  // Check Access Right Here
  // Clear itemController
  itemModel.deleteMany({})
  .exec((err, result) =>{
    if(result.deletedCount > 0){
      res.send({"result": "ok"});
    } else {
      res.send({"result": "error"});
    }
  })
  // End of Clear itemController
  // End of Check Access Right
}