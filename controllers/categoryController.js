/*
 file name: categoryControllers.js
 description: Handle RESTful API for Category
*/
var categoryModel = require('../models/Categories');

exports.getAllCategory = function(req, res){
 // Find All Category
 categoryModel.find({})
 .exec((err, result) => {
  if(err)
   throw err;
  res.send({"result": "ok", "data": result});
 })
 // End of Finding
}

exports.getOneCategory = function(req, res){
 let name = req.params.name;
 if(name != ""){
  categoryModel.find({name: name})
  .exec((err, result) => {
   if(err)
    throw err;
   if(result.length > 0){
    res.send({"result": "ok", "data": result[0]});
   } else {
    res.send({"result": "error"});
   }
  })
 } else {
  res.send({"result": "error"});
 }
}

exports.addOneCategory = function(req, res){

}

exports.addManyCategory = function(req, res){
 // Check Access Right Here
 // Insert Here
 let arr = req.body.categories;
 // Check Data
 if(arr instanceof Array){
  categoryModel.insertMany(arr, (err, result) => {
   if(err)
    throw err;
   if(result.length > 0){
    // Insert Successfully
    res.send({"result": "ok", "count": result.length});
   } else {
    // No insert succeed
    res.send({"result": "error", "error": "failed"});
   }
  });
 } else {
  // Input data error
  res.send({"result": "error", "error": "wrong data type"});
 } // End of data checking
 // End of Insert
 // End of Check Access Right
}

exports.clearDataBase = function(req, res){
 // Check Access Right Here
 // Delete Here
 categoryModel.deleteMany({})
 .exec((err, result) => {
  if(err)
   throw err;
  if(result.deletedCount > 0){
   res.send({"result": "ok"});
  } else {
   res.send({"result": "error"});
  }
 })
 // End of Delete
 // End of Check Access Right
}