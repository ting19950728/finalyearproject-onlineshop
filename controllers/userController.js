/*
 file name: userControllers.js
 description: Handle RESTful API for User
*/
var userModel = require('../models/User');
var crypto = require('crypto');

exports.login = function(req, res){
 // Check Logged in
 if(!req.session.account){
  let user = req.body;
  let username = user.username;
  let password = user.password;
  // Check input data
  if(username != "" && password != ""){
   // Login Here
   userModel.find({username: username})
   .exec((err, result) => {
    if(err)
     throw err;
    if(result.length > 0){
     let user = new userModel(result[0]);
     if(md5(password) == user.password){
      // set session here
      // return result
      req.session.account = user;
      res.send({"result": "ok"});
     } else {
      res.send({"result": "error"});
     }
    }
   }); // End of Login
  } else {
   res.send({"result": "error"});
  } // End of check data
 } else {
   res.send({"result": "error"});
 }// End of check login
}

exports.register = function(req, res){
 // Check Logged in
 if(!req.session.account){
  let data = req.body;
  let password = md5(req.body.password);
  let model = {
    username: data.username,
    password: password,
    firstName: data.firstName,
    lastName: data.lastName,
    gender: data.gender,
    age: data.age,
    habits: data['habits'] ? data['habits'] : data['habits[]']
  }
  console.log(model.habits);
  let user = new userModel(model);
  // Register Here
  user.save((err) => {
   if(err)
    throw err;
   // set session here
   // return result
   req.session.account = user;
   res.send({"result": "ok"});
  }); // End of Register
 } else {
  res.send({"result": "error"});
 }// End of Check Login
}

exports.logout = function(req, res){
 // erase session
 req.session.account = undefined;
}

exports.isLogin = function(req, res){
 // Check Logged In
 if(req.session.account){
  res.send({"result": "ok", "user": req.session.account});
 } else {
  res.send({"result": "error"});
 }
 // End of Check login
}

exports.logout = function(req, res){
  req.session.account = undefined;
  res.send({"result": "ok"});
}

exports.getAllUser = function(req, res){
 // Find All User
 userModel.find({})
 .exec((err, result) => {
  if(err)
   throw err;
  res.send({"result": "ok", "data": result});
 });
 // End of Find User
}

exports.getAllHabits = function(req, res){
  userModel.find({})
  .exec()
  .then((doc) => {
    let habits = [];
    doc.forEach((user) => {
      user.habits.forEach((habit) => {
        if(habits.indexOf(habit.toLowerCase()) == -1)
          habits.push(habit.toLowerCase());
      });
    });
    res.send({"result": "ok", "data": habits});
  })
  .catch((err) => {
    throw err;
  })
}

exports.addOneUser = function(req, res){
 // Check Access Right
 // End of Check Access Right
}

exports.addManyUser = function(req, res){
 // Check Access Right
 let arr  = req.body.users;
 // Check Data
 if(arr instanceof Array){
  // Insert Here
  for(let x in arr){
   arr[x].password = md5(arr[x].passowrd);
  }
  userModel.insertMany(arr)
  .exec((err, result) => {
   if(err)
    throw err;
   if(result.nInserted > 0){
    res.send({"result": "ok"});
   } else {
    res.send({"result": "error"});
   }
  });
  // End of Insert
 } else {
  res.send({"result": "error"});
 } // End of Check Data
 // End of Check Access Right
}

exports.clearDataBase = function(req, res){
 // Check Access Right
 // Delete Here
 userModel.deleteMany({})
 .exec((err, result) => {
  if(err)
   throw err;
  if(result.deletedCount > 0){
   res.send({"result": "ok"});
  } else {
   res.send({"result": "error"});
  }
 });
 // End of Delete
 // End of Check Access Right
}

function md5(str){
 var md5sum = crypto.createHash('md5');
 return md5sum.update(str).digest('hex');
}
